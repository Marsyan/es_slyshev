package service

import (
	"fmt"
	"sort"

	"gitlab.com/Marsyan/es_slyshev/internal/storage"
	"gitlab.com/Marsyan/es_slyshev/models"
)

var (
	langCheck = map[string]int64{
		"GO":     1123,
		"C#":     2321,
		"C++":    3312,
		"NodeJS": 4123,
		"Python": 5321,
	}
)

type Service struct {
	storage *storage.Storage
}

func NewService(storage *storage.Storage) *Service {
	return &Service{storage: storage}
}

func (s *Service) Yes1() {
	s.storage.Yes1()
}

func (s *Service) No1() {
	s.storage.No1()
}

func (s *Service) Yes2() {
	s.storage.Yes2()
}

func (s *Service) No2() {
	s.storage.No2()
}

func (s *Service) Yes3() {
	s.storage.Yes3()
}

func (s *Service) No3() {
	s.storage.No3()
}

func (s *Service) Yes4() {
	s.storage.Yes4()
}

func (s *Service) No4() {
	s.storage.No4()
}

func (s *Service) Yes5() {
	s.storage.Yes5()
}

func (s *Service) No5() {
	s.storage.No5()
}

func (s *Service) Yes6() {
	s.storage.Yes6()
}

func (s *Service) No6() {
	s.storage.No6()
}

func (s *Service) Yes7() {
	s.storage.Yes7()
}

func (s *Service) No7() {
	s.storage.No7()
}

func (s *Service) Yes8() {
	s.storage.Yes8()
}

func (s *Service) No8() {
	s.storage.No8()
}

func (s *Service) Yes9() {
	s.storage.Yes9()
}

func (s *Service) No9() {
	s.storage.No9()
}

func (s *Service) Yes10() {
	s.storage.Yes10()
}

func (s *Service) No10() {
	s.storage.No10()
}

func (s *Service) Yes11() {
	s.storage.Yes11()
}

func (s *Service) No11() {
	s.storage.No11()
}

func (s *Service) Yes12() {
	s.storage.Yes12()
}

func (s *Service) No12() {
	s.storage.No12()
}

func (s *Service) Yes13() {
	s.storage.Yes13()
}

func (s *Service) No13() {
	s.storage.No13()
}

func (s *Service) Yes14() {
	s.storage.Yes14()
}

func (s *Service) No14() {
	s.storage.No14()
}

func (s *Service) Yes15() {
	s.storage.Yes15()
}

func (s *Service) No15() {
	s.storage.No15()
}

func (s *Service) Yes16() {
	s.storage.Yes16()
}

func (s *Service) No16() {
	s.storage.No16()
}

func (s *Service) Yes17() {
	s.storage.Yes17()
}

func (s *Service) No17() {
	s.storage.No17()
}

func (s *Service) Yes18() {
	s.storage.Yes18()
}

func (s *Service) No18() {
	s.storage.No18()
}

func (s *Service) Yes19() {
	s.storage.Yes19()
}

func (s *Service) No19() {
	s.storage.No19()
}

func (s *Service) Yes20() {
	s.storage.Yes20()
}

func (s *Service) No20() {
	s.storage.No20()
}

func (s *Service) Yes21() {
	s.storage.Yes21()
}

func (s *Service) No21() {
	s.storage.No21()
}

func (s *Service) GetTop() string {
	fromDB := s.storage.GetAll()
	return getTotal(fromDB)
}

func getTotal(languages map[string]models.Language) string {
	result := make(map[float64]string, 5)
	ints := make([]float64, 0, 5)
	for key, value := range languages {
		values := value.Ease + value.BreadthOfApplications + value.EntryThreshold +
			+value.HumanReadability + value.Capabilities + value.Modernity

		result[values] = key
		ints = append(ints, values)
	}

	sort.Sort(sort.Reverse(sort.Float64Slice(ints)))

	top := fmt.Sprint("Top:\t1. " + result[ints[0]] + "\t2. " + result[ints[1]] + "\t3. " + result[ints[2]] +
		"\t4. " + result[ints[3]] + "\t5. " + result[ints[4]])
	return top
}
