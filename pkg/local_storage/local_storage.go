package local_storage

import (
	"gitlab.com/Marsyan/es_slyshev/models"
)

var (
	storage = make(map[string]models.Language, 5)
)

func NewDB() map[string]models.Language {
	return storage
}
