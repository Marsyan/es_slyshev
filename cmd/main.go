package main

import (
	delivery2 "gitlab.com/Marsyan/es_slyshev/internal/delivery"
	service2 "gitlab.com/Marsyan/es_slyshev/internal/service"
	storage2 "gitlab.com/Marsyan/es_slyshev/internal/storage"
	"gitlab.com/Marsyan/es_slyshev/pkg/local_storage"
	"gitlab.com/Marsyan/es_slyshev/pkg/tg_connection"
)

func main() {
	// root:dbpass@tcp(127.0.0.1:3306)/es_slysev
	db := local_storage.NewDB()
	storage := storage2.NewStorage(db)
	service := service2.NewService(storage)
	delivery := delivery2.NewDelivery(service)
	tgApi := tg_connection.NewTGApi(delivery)

	tgApi.StartTGBot()
}
