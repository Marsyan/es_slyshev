package storage

import (
	"gitlab.com/Marsyan/es_slyshev/models"
)

type Storage struct {
	db map[string]models.Language
}

// GO, Python, C#, C++, Node JS, etc
func NewStorage(db map[string]models.Language) *Storage {
	db = make(map[string]models.Language)
	return &Storage{db: db}
}

func (s *Storage) Yes1() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Ease = 3.01
	s.db["NodeJS"] = nodeJS

	python := s.db["Python"]
	python.Ease = 4.02
	s.db["Python"] = python

	golang := s.db["GO"]
	golang.Ease = 5.03
	s.db["GO"] = golang

	cPlusPlus := s.db["C++"]
	cPlusPlus.Ease = 2.04
	s.db["C++"] = cPlusPlus

	cSharp := s.db["C#"]
	cSharp.Ease = 1.05
	s.db["C#"] = cSharp

	bash := s.db["Bash"]
	bash.Ease += 3.06
	s.db["Bash"] = bash

	cShell := s.db["C_SHELL"]
	cShell.Ease += 3.07
	s.db["C_SHELL"] = cShell

	java := s.db["Java"]
	java.Ease += 3.08
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.Ease += 2.09
	s.db["JavaScript"] = js

	kotlin := s.db["Kotlin"]
	kotlin.Ease += 1.1
	s.db["Kotlin"] = kotlin

	perl := s.db["Perl"]
	perl.Ease += 1.11
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.Ease += 3.12
	s.db["Pascal"] = pas

	php := s.db["PHP"]
	php.Ease += 3.13
	s.db["PHP"] = php

	ruby := s.db["Ruby"]
	ruby.Ease += 2.14
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Ease += 1.15
	s.db["SQL"] = sql
}

func (s *Storage) No1() {
	cPlusPlus := s.db["C++"]
	cPlusPlus.EntryThreshold = 3.01
	s.db["C++"] = cPlusPlus

	cSharp := s.db["C#"]
	cSharp.EntryThreshold = 2.02
	s.db["C#"] = cSharp

	golang := s.db["GO"]
	golang.EntryThreshold = 5.03
	s.db["GO"] = golang

	nodeJS := s.db["NodeJS"]
	nodeJS.Ease = 4.04
	s.db["NodeJS"] = nodeJS

	python := s.db["Python"]
	python.EntryThreshold = 1.05
	s.db["Python"] = python

	bash := s.db["Bash"]
	bash.EntryThreshold += 3.06
	s.db["Bash"] = bash

	cShell := s.db["C_SHELL"]
	cShell.EntryThreshold += 3.07
	s.db["C_SHELL"] = cShell

	java := s.db["Java"]
	java.EntryThreshold += 3.08
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.EntryThreshold += 2.09
	s.db["JavaScript"] = js

	kotlin := s.db["Kotlin"]
	kotlin.EntryThreshold += 1.1
	s.db["Kotlin"] = kotlin

	perl := s.db["Perl"]
	perl.EntryThreshold += 1.11
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.EntryThreshold += 4.12
	s.db["Pascal"] = pas

	php := s.db["PHP"]
	php.EntryThreshold += 3.13
	s.db["PHP"] = php

	ruby := s.db["Ruby"]
	ruby.EntryThreshold += 2.14
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Ease += 1.15
	s.db["SQL"] = sql
}

func (s *Storage) No2() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Capabilities += 2
	nodeJS.EntryThreshold += 3
	s.db["NodeJS"] = nodeJS

	python := s.db["Python"]
	python.Capabilities += 2
	python.EntryThreshold += 4
	s.db["Python"] = python

	golang := s.db["GO"]
	golang.Capabilities += 5
	golang.EntryThreshold += 3
	s.db["GO"] = golang

	bash := s.db["Bash"]
	bash.Capabilities += 3
	bash.EntryThreshold += 4
	s.db["Bash"] = bash

	js := s.db["JavaScript"]
	js.Capabilities += 1
	js.EntryThreshold += 2
	s.db["JavaScript"] = js

	pas := s.db["Pascal"]
	pas.EntryThreshold += 2
	pas.Capabilities += 4
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Capabilities += 2
	ruby.EntryThreshold += 4
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Capabilities += 1
	sql.EntryThreshold += 4
	s.db["SQL"] = sql
}

func (s *Storage) Yes2() {
	cPlusPlus := s.db["C++"]
	cPlusPlus.Capabilities += 3
	cPlusPlus.EntryThreshold += 1
	s.db["C++"] = cPlusPlus

	cSharp := s.db["C#"]
	cSharp.Capabilities += 4
	cSharp.EntryThreshold += 2
	s.db["C#"] = cSharp

	cShell := s.db["C_SHELL"]
	cShell.EntryThreshold += 3
	cShell.Capabilities += 1
	s.db["C_SHELL"] = cShell

	java := s.db["Java"]
	java.EntryThreshold += 3
	java.Capabilities += 2
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.EntryThreshold += 3
	kotlin.Capabilities += 2
	s.db["Kotlin"] = kotlin

	perl := s.db["Perl"]
	perl.EntryThreshold += 3
	perl.Capabilities += 1
	s.db["Perl"] = perl

	php := s.db["PHP"]
	php.EntryThreshold += 5
	php.Capabilities += 1
	s.db["PHP"] = php
}

func (s *Storage) No3() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Capabilities += 1
	s.db["NodeJS"] = nodeJS

	python := s.db["C#"]
	python.Capabilities += 1
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Capabilities += 1
	s.db["Bash"] = bash

	cShell := s.db["C_SHELL"]
	cShell.Capabilities += 1
	s.db["C_SHELL"] = cShell

	kotlin := s.db["Kotlin"]
	kotlin.Capabilities += 2
	s.db["Kotlin"] = kotlin

	pas := s.db["Pascal"]
	pas.Capabilities += 1
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Capabilities += 3
	s.db["Ruby"] = ruby
}

func (s *Storage) Yes3() {
	Python := s.db["C++"]
	Python.Capabilities += 3
	s.db["C++"] = Python

	cSharp := s.db["Python"]
	cSharp.Capabilities += 1
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.Capabilities += 1
	s.db["GO"] = golang

	java := s.db["Java"]
	java.Capabilities += 3
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.Capabilities += 3
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.Capabilities += 1
	s.db["Perl"] = perl

	php := s.db["PHP"]
	php.Capabilities += 1
	s.db["PHP"] = php

	sql := s.db["SQL"]
	sql.Capabilities += 1
	s.db["SQL"] = sql
}

func (s *Storage) No4() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Compilability += 3
	s.db["NodeJS"] = nodeJS

	python := s.db["C#"]
	python.Compilability += 3
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Compilability += 3
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.Compilability += 3
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.Compilability += 2
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.Compilability += 1
	s.db["PHP"] = php
}

func (s *Storage) Yes4() {
	Python := s.db["C++"]
	Python.Performance += 3
	s.db["C++"] = Python

	cSharp := s.db["Python"]
	cSharp.Performance += 1
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.Performance += 2
	s.db["GO"] = golang

	cShell := s.db["C_SHELL"]
	cShell.Performance += 3
	s.db["C_SHELL"] = cShell

	js := s.db["JavaScript"]
	js.Performance += 1
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.Performance += 3
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.Performance += 2
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Performance += 1
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Performance += 1
	s.db["SQL"] = sql
}

func (s *Storage) No5() {
	cPlus := s.db["C++"]
	cPlus.Crossplatform += 3
	s.db["C++"] = cPlus

	python := s.db["C#"]
	python.Crossplatform += 2
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Crossplatform += 1
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.Crossplatform += 2
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.Crossplatform += 1
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.Crossplatform += 1
	s.db["PHP"] = php
}

func (s *Storage) Yes5() {
	cSharp := s.db["Python"]
	cSharp.HumanReadability += 1
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.HumanReadability += 1
	s.db["GO"] = golang

	nodeJS := s.db["NodeJS"]
	nodeJS.HumanReadability += 1
	s.db["NodeJS"] = nodeJS

	cShell := s.db["C_SHELL"]
	cShell.HumanReadability += 3
	s.db["C_SHELL"] = cShell

	js := s.db["JavaScript"]
	js.HumanReadability += 2
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.HumanReadability += 3
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.HumanReadability += 2
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.HumanReadability += 2
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.HumanReadability += 1
	s.db["SQL"] = sql
}

func (s *Storage) No6() {
	cPlus := s.db["NodeJS"]
	cPlus.DemandInTheMarket += 3
	s.db["NodeJS"] = cPlus

	python := s.db["C#"]
	python.DemandInTheMarket += 1
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.DemandInTheMarket += 1
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.DemandInTheMarket += 2
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.DemandInTheMarket += 3
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.DemandInTheMarket += 3
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.DemandInTheMarket += 2
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.DemandInTheMarket += 1
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.DemandInTheMarket += 4
	s.db["SQL"] = sql
}

func (s *Storage) Yes6() {
	cPlus := s.db["C++"]
	cPlus.DemandInTheMarket += 7
	s.db["C++"] = cPlus

	cSharp := s.db["Python"]
	cSharp.DemandInTheMarket += 6
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.DemandInTheMarket += 5
	s.db["GO"] = golang

	cShell := s.db["C_SHELL"]
	cShell.DemandInTheMarket += 3
	s.db["C_SHELL"] = cShell

	kotlin := s.db["Kotlin"]
	kotlin.DemandInTheMarket += 3
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.DemandInTheMarket += 2
	s.db["PHP"] = php
}

func (s *Storage) No7() {
	cPlus := s.db["NodeJS"]
	cPlus.Modernity += 4
	cPlus.Crossplatform += 3
	cPlus.Capabilities += 3
	s.db["NodeJS"] = cPlus

	python := s.db["C#"]
	python.Modernity += 2
	python.Crossplatform += 5
	python.Capabilities += 1
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Modernity += 2
	bash.Crossplatform += 5
	bash.Capabilities += 1
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.Modernity += 3
	java.Capabilities += 2
	java.Crossplatform += 2
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.Modernity += 3
	kotlin.Capabilities += 1
	kotlin.Crossplatform += 2
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.Compilability += 1
	php.Modernity += 1
	php.Crossplatform += 1
	s.db["PHP"] = php

	sql := s.db["SQL"]
	sql.Capabilities += 1
	sql.Modernity += 4
	sql.Crossplatform += 3
	s.db["SQL"] = sql
}

func (s *Storage) Yes7() {
	cSharp := s.db["Python"]
	cSharp.Modernity += 4
	cSharp.Crossplatform += 7
	cSharp.Capabilities += 10
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.Modernity += 10
	golang.Crossplatform += 1
	golang.Capabilities += 8
	s.db["GO"] = golang

	cPlus := s.db["C++"]
	cPlus.Modernity += 1
	cPlus.Crossplatform += 5
	cPlus.Capabilities += 8
	s.db["C++"] = cPlus

	cShell := s.db["C_SHELL"]
	cShell.Modernity += 3
	cShell.Capabilities += 1
	cShell.Crossplatform += 5
	s.db["C_SHELL"] = cShell

	js := s.db["JavaScript"]
	js.Capabilities += 5
	js.Modernity += 2
	js.Crossplatform += 3
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.Capabilities += 3
	perl.Modernity += 1
	perl.Crossplatform += 1
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.Capabilities += 2
	pas.Crossplatform += 2
	pas.Modernity += 1
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Capabilities += 1
	ruby.Crossplatform += 3
	ruby.Modernity += 2
	s.db["Ruby"] = ruby
}

func (s *Storage) Yes8() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Ease = 3.01
	s.db["NodeJS"] = nodeJS

	python := s.db["Python"]
	python.Ease = 4.02
	s.db["Python"] = python

	golang := s.db["GO"]
	golang.Ease = 5.03
	s.db["GO"] = golang

	cPlusPlus := s.db["C++"]
	cPlusPlus.Ease = 2.04
	s.db["C++"] = cPlusPlus

	cSharp := s.db["C#"]
	cSharp.Ease = 1.05
	s.db["C#"] = cSharp

	bash := s.db["Bash"]
	bash.Ease += 3.06
	s.db["Bash"] = bash

	cShell := s.db["C_SHELL"]
	cShell.Ease += 3.07
	s.db["C_SHELL"] = cShell

	java := s.db["Java"]
	java.Ease += 3.08
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.Ease += 2.09
	s.db["JavaScript"] = js

	kotlin := s.db["Kotlin"]
	kotlin.Ease += 1.1
	s.db["Kotlin"] = kotlin

	perl := s.db["Perl"]
	perl.Ease += 1.11
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.Ease += 3.12
	s.db["Pascal"] = pas

	php := s.db["PHP"]
	php.Ease += 3.13
	s.db["PHP"] = php

	ruby := s.db["Ruby"]
	ruby.Ease += 2.14
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Ease += 1.15
	s.db["SQL"] = sql
}

func (s *Storage) No8() {
	cPlusPlus := s.db["C++"]
	cPlusPlus.EntryThreshold = 3.01
	s.db["C++"] = cPlusPlus

	cSharp := s.db["C#"]
	cSharp.EntryThreshold = 2.02
	s.db["C#"] = cSharp

	golang := s.db["GO"]
	golang.EntryThreshold = 5.03
	s.db["GO"] = golang

	nodeJS := s.db["NodeJS"]
	nodeJS.Ease = 4.04
	s.db["NodeJS"] = nodeJS

	python := s.db["Python"]
	python.EntryThreshold = 1.05
	s.db["Python"] = python

	bash := s.db["Bash"]
	bash.EntryThreshold += 3.06
	s.db["Bash"] = bash

	cShell := s.db["C_SHELL"]
	cShell.EntryThreshold += 3.07
	s.db["C_SHELL"] = cShell

	java := s.db["Java"]
	java.EntryThreshold += 3.08
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.EntryThreshold += 2.09
	s.db["JavaScript"] = js

	kotlin := s.db["Kotlin"]
	kotlin.EntryThreshold += 1.1
	s.db["Kotlin"] = kotlin

	perl := s.db["Perl"]
	perl.EntryThreshold += 1.11
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.EntryThreshold += 4.12
	s.db["Pascal"] = pas

	php := s.db["PHP"]
	php.EntryThreshold += 3.13
	s.db["PHP"] = php

	ruby := s.db["Ruby"]
	ruby.EntryThreshold += 2.14
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Ease += 1.15
	s.db["SQL"] = sql
}

func (s *Storage) No9() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Capabilities += 2
	nodeJS.EntryThreshold += 3
	s.db["NodeJS"] = nodeJS

	python := s.db["Python"]
	python.Capabilities += 2
	python.EntryThreshold += 4
	s.db["Python"] = python

	golang := s.db["GO"]
	golang.Capabilities += 5
	golang.EntryThreshold += 3
	s.db["GO"] = golang

	bash := s.db["Bash"]
	bash.Capabilities += 3
	bash.EntryThreshold += 4
	s.db["Bash"] = bash

	js := s.db["JavaScript"]
	js.Capabilities += 1
	js.EntryThreshold += 2
	s.db["JavaScript"] = js

	pas := s.db["Pascal"]
	pas.EntryThreshold += 2
	pas.Capabilities += 4
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Capabilities += 2
	ruby.EntryThreshold += 4
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Capabilities += 1
	sql.EntryThreshold += 4
	s.db["SQL"] = sql
}

func (s *Storage) Yes9() {
	cPlusPlus := s.db["C++"]
	cPlusPlus.Capabilities += 3
	cPlusPlus.EntryThreshold += 1
	s.db["C++"] = cPlusPlus

	cSharp := s.db["C#"]
	cSharp.Capabilities += 4
	cSharp.EntryThreshold += 2
	s.db["C#"] = cSharp

	cShell := s.db["C_SHELL"]
	cShell.EntryThreshold += 3
	cShell.Capabilities += 1
	s.db["C_SHELL"] = cShell

	java := s.db["Java"]
	java.EntryThreshold += 3
	java.Capabilities += 2
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.EntryThreshold += 3
	kotlin.Capabilities += 2
	s.db["Kotlin"] = kotlin

	perl := s.db["Perl"]
	perl.EntryThreshold += 3
	perl.Capabilities += 1
	s.db["Perl"] = perl

	php := s.db["PHP"]
	php.EntryThreshold += 5
	php.Capabilities += 1
	s.db["PHP"] = php
}

func (s *Storage) No10() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Capabilities += 1
	s.db["NodeJS"] = nodeJS

	python := s.db["C#"]
	python.Capabilities += 1
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Capabilities += 1
	s.db["Bash"] = bash

	cShell := s.db["C_SHELL"]
	cShell.Capabilities += 1
	s.db["C_SHELL"] = cShell

	kotlin := s.db["Kotlin"]
	kotlin.Capabilities += 2
	s.db["Kotlin"] = kotlin

	pas := s.db["Pascal"]
	pas.Capabilities += 1
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Capabilities += 3
	s.db["Ruby"] = ruby
}

func (s *Storage) Yes10() {
	Python := s.db["C++"]
	Python.Capabilities += 3
	s.db["C++"] = Python

	cSharp := s.db["Python"]
	cSharp.Capabilities += 1
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.Capabilities += 1
	s.db["GO"] = golang

	java := s.db["Java"]
	java.Capabilities += 3
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.Capabilities += 3
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.Capabilities += 1
	s.db["Perl"] = perl

	php := s.db["PHP"]
	php.Capabilities += 1
	s.db["PHP"] = php

	sql := s.db["SQL"]
	sql.Capabilities += 1
	s.db["SQL"] = sql
}

func (s *Storage) No11() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Compilability += 3
	s.db["NodeJS"] = nodeJS

	python := s.db["C#"]
	python.Compilability += 3
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Compilability += 3
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.Compilability += 3
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.Compilability += 2
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.Compilability += 1
	s.db["PHP"] = php
}

func (s *Storage) Yes11() {
	Python := s.db["C++"]
	Python.Performance += 3
	s.db["C++"] = Python

	cSharp := s.db["Python"]
	cSharp.Performance += 1
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.Performance += 2
	s.db["GO"] = golang

	cShell := s.db["C_SHELL"]
	cShell.Performance += 3
	s.db["C_SHELL"] = cShell

	js := s.db["JavaScript"]
	js.Performance += 1
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.Performance += 3
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.Performance += 2
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Performance += 1
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Performance += 1
	s.db["SQL"] = sql
}

func (s *Storage) No12() {
	cPlus := s.db["C++"]
	cPlus.Crossplatform += 3
	s.db["C++"] = cPlus

	python := s.db["C#"]
	python.Crossplatform += 2
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Crossplatform += 1
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.Crossplatform += 2
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.Crossplatform += 1
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.Crossplatform += 1
	s.db["PHP"] = php
}

func (s *Storage) Yes12() {
	cSharp := s.db["Python"]
	cSharp.HumanReadability += 1
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.HumanReadability += 1
	s.db["GO"] = golang

	nodeJS := s.db["NodeJS"]
	nodeJS.HumanReadability += 1
	s.db["NodeJS"] = nodeJS

	cShell := s.db["C_SHELL"]
	cShell.HumanReadability += 3
	s.db["C_SHELL"] = cShell

	js := s.db["JavaScript"]
	js.HumanReadability += 2
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.HumanReadability += 3
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.HumanReadability += 2
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.HumanReadability += 2
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.HumanReadability += 1
	s.db["SQL"] = sql
}

func (s *Storage) No13() {
	cPlus := s.db["NodeJS"]
	cPlus.DemandInTheMarket += 3
	s.db["NodeJS"] = cPlus

	python := s.db["C#"]
	python.DemandInTheMarket += 1
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.DemandInTheMarket += 1
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.DemandInTheMarket += 2
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.DemandInTheMarket += 3
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.DemandInTheMarket += 3
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.DemandInTheMarket += 2
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.DemandInTheMarket += 1
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.DemandInTheMarket += 4
	s.db["SQL"] = sql
}

func (s *Storage) Yes13() {
	cPlus := s.db["C++"]
	cPlus.DemandInTheMarket += 7
	s.db["C++"] = cPlus

	cSharp := s.db["Python"]
	cSharp.DemandInTheMarket += 6
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.DemandInTheMarket += 5
	s.db["GO"] = golang

	cShell := s.db["C_SHELL"]
	cShell.DemandInTheMarket += 3
	s.db["C_SHELL"] = cShell

	kotlin := s.db["Kotlin"]
	kotlin.DemandInTheMarket += 3
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.DemandInTheMarket += 2
	s.db["PHP"] = php
}

func (s *Storage) No14() {
	cPlus := s.db["NodeJS"]
	cPlus.Modernity += 4
	cPlus.Crossplatform += 3
	cPlus.Capabilities += 3
	s.db["NodeJS"] = cPlus

	python := s.db["C#"]
	python.Modernity += 2
	python.Crossplatform += 5
	python.Capabilities += 1
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Modernity += 2
	bash.Crossplatform += 5
	bash.Capabilities += 1
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.Modernity += 3
	java.Capabilities += 2
	java.Crossplatform += 2
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.Modernity += 3
	kotlin.Capabilities += 1
	kotlin.Crossplatform += 2
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.Compilability += 1
	php.Modernity += 1
	php.Crossplatform += 1
	s.db["PHP"] = php

	sql := s.db["SQL"]
	sql.Capabilities += 1
	sql.Modernity += 4
	sql.Crossplatform += 3
	s.db["SQL"] = sql
}

func (s *Storage) Yes14() {
	cSharp := s.db["Python"]
	cSharp.Modernity += 4
	cSharp.Crossplatform += 7
	cSharp.Capabilities += 10
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.Modernity += 10
	golang.Crossplatform += 1
	golang.Capabilities += 8
	s.db["GO"] = golang

	cPlus := s.db["C++"]
	cPlus.Modernity += 1
	cPlus.Crossplatform += 5
	cPlus.Capabilities += 8
	s.db["C++"] = cPlus

	cShell := s.db["C_SHELL"]
	cShell.Modernity += 3
	cShell.Capabilities += 1
	cShell.Crossplatform += 5
	s.db["C_SHELL"] = cShell

	js := s.db["JavaScript"]
	js.Capabilities += 5
	js.Modernity += 2
	js.Crossplatform += 3
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.Capabilities += 3
	perl.Modernity += 1
	perl.Crossplatform += 1
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.Capabilities += 2
	pas.Crossplatform += 2
	pas.Modernity += 1
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Capabilities += 1
	ruby.Crossplatform += 3
	ruby.Modernity += 2
	s.db["Ruby"] = ruby
}

func (s *Storage) Yes15() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Ease = 3.01
	s.db["NodeJS"] = nodeJS

	python := s.db["Python"]
	python.Ease = 4.02
	s.db["Python"] = python

	golang := s.db["GO"]
	golang.Ease = 5.03
	s.db["GO"] = golang

	cPlusPlus := s.db["C++"]
	cPlusPlus.Ease = 2.04
	s.db["C++"] = cPlusPlus

	cSharp := s.db["C#"]
	cSharp.Ease = 1.05
	s.db["C#"] = cSharp

	bash := s.db["Bash"]
	bash.Ease += 3.06
	s.db["Bash"] = bash

	cShell := s.db["C_SHELL"]
	cShell.Ease += 3.07
	s.db["C_SHELL"] = cShell

	java := s.db["Java"]
	java.Ease += 3.08
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.Ease += 2.09
	s.db["JavaScript"] = js

	kotlin := s.db["Kotlin"]
	kotlin.Ease += 1.1
	s.db["Kotlin"] = kotlin

	perl := s.db["Perl"]
	perl.Ease += 1.11
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.Ease += 3.12
	s.db["Pascal"] = pas

	php := s.db["PHP"]
	php.Ease += 3.13
	s.db["PHP"] = php

	ruby := s.db["Ruby"]
	ruby.Ease += 2.14
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Ease += 1.15
	s.db["SQL"] = sql
}

func (s *Storage) No15() {
	cPlusPlus := s.db["C++"]
	cPlusPlus.EntryThreshold = 3.01
	s.db["C++"] = cPlusPlus

	cSharp := s.db["C#"]
	cSharp.EntryThreshold = 2.02
	s.db["C#"] = cSharp

	golang := s.db["GO"]
	golang.EntryThreshold = 5.03
	s.db["GO"] = golang

	nodeJS := s.db["NodeJS"]
	nodeJS.Ease = 4.04
	s.db["NodeJS"] = nodeJS

	python := s.db["Python"]
	python.EntryThreshold = 1.05
	s.db["Python"] = python

	bash := s.db["Bash"]
	bash.EntryThreshold += 3.06
	s.db["Bash"] = bash

	cShell := s.db["C_SHELL"]
	cShell.EntryThreshold += 3.07
	s.db["C_SHELL"] = cShell

	java := s.db["Java"]
	java.EntryThreshold += 3.08
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.EntryThreshold += 2.09
	s.db["JavaScript"] = js

	kotlin := s.db["Kotlin"]
	kotlin.EntryThreshold += 1.1
	s.db["Kotlin"] = kotlin

	perl := s.db["Perl"]
	perl.EntryThreshold += 1.11
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.EntryThreshold += 4.12
	s.db["Pascal"] = pas

	php := s.db["PHP"]
	php.EntryThreshold += 3.13
	s.db["PHP"] = php

	ruby := s.db["Ruby"]
	ruby.EntryThreshold += 2.14
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Ease += 1.15
	s.db["SQL"] = sql
}

func (s *Storage) No16() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Capabilities += 2
	nodeJS.EntryThreshold += 3
	s.db["NodeJS"] = nodeJS

	python := s.db["Python"]
	python.Capabilities += 2
	python.EntryThreshold += 4
	s.db["Python"] = python

	golang := s.db["GO"]
	golang.Capabilities += 5
	golang.EntryThreshold += 3
	s.db["GO"] = golang

	bash := s.db["Bash"]
	bash.Capabilities += 3
	bash.EntryThreshold += 4
	s.db["Bash"] = bash

	js := s.db["JavaScript"]
	js.Capabilities += 1
	js.EntryThreshold += 2
	s.db["JavaScript"] = js

	pas := s.db["Pascal"]
	pas.EntryThreshold += 2
	pas.Capabilities += 4
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Capabilities += 2
	ruby.EntryThreshold += 4
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Capabilities += 1
	sql.EntryThreshold += 4
	s.db["SQL"] = sql
}

func (s *Storage) Yes16() {
	cPlusPlus := s.db["C++"]
	cPlusPlus.Capabilities += 3
	cPlusPlus.EntryThreshold += 1
	s.db["C++"] = cPlusPlus

	cSharp := s.db["C#"]
	cSharp.Capabilities += 4
	cSharp.EntryThreshold += 2
	s.db["C#"] = cSharp

	cShell := s.db["C_SHELL"]
	cShell.EntryThreshold += 3
	cShell.Capabilities += 1
	s.db["C_SHELL"] = cShell

	java := s.db["Java"]
	java.EntryThreshold += 3
	java.Capabilities += 2
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.EntryThreshold += 3
	kotlin.Capabilities += 2
	s.db["Kotlin"] = kotlin

	perl := s.db["Perl"]
	perl.EntryThreshold += 3
	perl.Capabilities += 1
	s.db["Perl"] = perl

	php := s.db["PHP"]
	php.EntryThreshold += 5
	php.Capabilities += 1
	s.db["PHP"] = php
}

func (s *Storage) No17() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Capabilities += 1
	s.db["NodeJS"] = nodeJS

	python := s.db["C#"]
	python.Capabilities += 1
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Capabilities += 1
	s.db["Bash"] = bash

	cShell := s.db["C_SHELL"]
	cShell.Capabilities += 1
	s.db["C_SHELL"] = cShell

	kotlin := s.db["Kotlin"]
	kotlin.Capabilities += 2
	s.db["Kotlin"] = kotlin

	pas := s.db["Pascal"]
	pas.Capabilities += 1
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Capabilities += 3
	s.db["Ruby"] = ruby
}

func (s *Storage) Yes17() {
	Python := s.db["C++"]
	Python.Capabilities += 3
	s.db["C++"] = Python

	cSharp := s.db["Python"]
	cSharp.Capabilities += 1
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.Capabilities += 1
	s.db["GO"] = golang

	java := s.db["Java"]
	java.Capabilities += 3
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.Capabilities += 3
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.Capabilities += 1
	s.db["Perl"] = perl

	php := s.db["PHP"]
	php.Capabilities += 1
	s.db["PHP"] = php

	sql := s.db["SQL"]
	sql.Capabilities += 1
	s.db["SQL"] = sql
}

func (s *Storage) No18() {
	nodeJS := s.db["NodeJS"]
	nodeJS.Compilability += 3
	s.db["NodeJS"] = nodeJS

	python := s.db["C#"]
	python.Compilability += 3
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Compilability += 3
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.Compilability += 3
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.Compilability += 2
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.Compilability += 1
	s.db["PHP"] = php
}

func (s *Storage) Yes18() {
	Python := s.db["C++"]
	Python.Performance += 3
	s.db["C++"] = Python

	cSharp := s.db["Python"]
	cSharp.Performance += 1
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.Performance += 2
	s.db["GO"] = golang

	cShell := s.db["C_SHELL"]
	cShell.Performance += 3
	s.db["C_SHELL"] = cShell

	js := s.db["JavaScript"]
	js.Performance += 1
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.Performance += 3
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.Performance += 2
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Performance += 1
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.Performance += 1
	s.db["SQL"] = sql
}

func (s *Storage) No19() {
	cPlus := s.db["C++"]
	cPlus.Crossplatform += 3
	s.db["C++"] = cPlus

	python := s.db["C#"]
	python.Crossplatform += 2
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Crossplatform += 1
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.Crossplatform += 2
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.Crossplatform += 1
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.Crossplatform += 1
	s.db["PHP"] = php
}

func (s *Storage) Yes19() {
	cSharp := s.db["Python"]
	cSharp.HumanReadability += 1
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.HumanReadability += 1
	s.db["GO"] = golang

	nodeJS := s.db["NodeJS"]
	nodeJS.HumanReadability += 1
	s.db["NodeJS"] = nodeJS

	cShell := s.db["C_SHELL"]
	cShell.HumanReadability += 3
	s.db["C_SHELL"] = cShell

	js := s.db["JavaScript"]
	js.HumanReadability += 2
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.HumanReadability += 3
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.HumanReadability += 2
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.HumanReadability += 2
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.HumanReadability += 1
	s.db["SQL"] = sql
}

func (s *Storage) No20() {
	cPlus := s.db["NodeJS"]
	cPlus.DemandInTheMarket += 3
	s.db["NodeJS"] = cPlus

	python := s.db["C#"]
	python.DemandInTheMarket += 1
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.DemandInTheMarket += 1
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.DemandInTheMarket += 2
	s.db["Java"] = java

	js := s.db["JavaScript"]
	js.DemandInTheMarket += 3
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.DemandInTheMarket += 3
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.DemandInTheMarket += 2
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.DemandInTheMarket += 1
	s.db["Ruby"] = ruby

	sql := s.db["SQL"]
	sql.DemandInTheMarket += 4
	s.db["SQL"] = sql
}

func (s *Storage) Yes20() {
	cPlus := s.db["C++"]
	cPlus.DemandInTheMarket += 7
	s.db["C++"] = cPlus

	cSharp := s.db["Python"]
	cSharp.DemandInTheMarket += 6
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.DemandInTheMarket += 5
	s.db["GO"] = golang

	cShell := s.db["C_SHELL"]
	cShell.DemandInTheMarket += 3
	s.db["C_SHELL"] = cShell

	kotlin := s.db["Kotlin"]
	kotlin.DemandInTheMarket += 3
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.DemandInTheMarket += 2
	s.db["PHP"] = php
}

func (s *Storage) No21() {
	cPlus := s.db["NodeJS"]
	cPlus.Modernity += 4
	cPlus.Crossplatform += 3
	cPlus.Capabilities += 3
	s.db["NodeJS"] = cPlus

	python := s.db["C#"]
	python.Modernity += 2
	python.Crossplatform += 5
	python.Capabilities += 1
	s.db["C#"] = python

	bash := s.db["Bash"]
	bash.Modernity += 2
	bash.Crossplatform += 5
	bash.Capabilities += 1
	s.db["Bash"] = bash

	java := s.db["Java"]
	java.Modernity += 3
	java.Capabilities += 2
	java.Crossplatform += 2
	s.db["Java"] = java

	kotlin := s.db["Kotlin"]
	kotlin.Modernity += 3
	kotlin.Capabilities += 1
	kotlin.Crossplatform += 2
	s.db["Kotlin"] = kotlin

	php := s.db["PHP"]
	php.Compilability += 1
	php.Modernity += 1
	php.Crossplatform += 1
	s.db["PHP"] = php

	sql := s.db["SQL"]
	sql.Capabilities += 1
	sql.Modernity += 4
	sql.Crossplatform += 3
	s.db["SQL"] = sql
}

func (s *Storage) Yes21() {
	cSharp := s.db["Python"]
	cSharp.Modernity += 4
	cSharp.Crossplatform += 7
	cSharp.Capabilities += 10
	s.db["Python"] = cSharp

	golang := s.db["GO"]
	golang.Modernity += 10
	golang.Crossplatform += 1
	golang.Capabilities += 8
	s.db["GO"] = golang

	cPlus := s.db["C++"]
	cPlus.Modernity += 1
	cPlus.Crossplatform += 5
	cPlus.Capabilities += 8
	s.db["C++"] = cPlus

	cShell := s.db["C_SHELL"]
	cShell.Modernity += 3
	cShell.Capabilities += 1
	cShell.Crossplatform += 5
	s.db["C_SHELL"] = cShell

	js := s.db["JavaScript"]
	js.Capabilities += 5
	js.Modernity += 2
	js.Crossplatform += 3
	s.db["JavaScript"] = js

	perl := s.db["Perl"]
	perl.Capabilities += 3
	perl.Modernity += 1
	perl.Crossplatform += 1
	s.db["Perl"] = perl

	pas := s.db["Pascal"]
	pas.Capabilities += 2
	pas.Crossplatform += 2
	pas.Modernity += 1
	s.db["Pascal"] = pas

	ruby := s.db["Ruby"]
	ruby.Capabilities += 1
	ruby.Crossplatform += 3
	ruby.Modernity += 2
	s.db["Ruby"] = ruby
}
func (s *Storage) GetAll() map[string]models.Language {
	return s.db
}
