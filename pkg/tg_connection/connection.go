package tg_connection

import (
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/Marsyan/es_slyshev/internal/delivery"
)

type TGApi struct {
	delivery *delivery.Delivery
}

func NewTGApi(delivery *delivery.Delivery) *TGApi {
	return &TGApi{delivery: delivery}
}

var numericKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonURL("Golang", "http://google.com"),
		tgbotapi.NewInlineKeyboardButtonURL("C#", "http://google.com"),
		tgbotapi.NewInlineKeyboardButtonURL("Python", "http://google.com"),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonURL("C++", "http://google.com"),
		tgbotapi.NewInlineKeyboardButtonURL("Node JS", "http://google.com"),
		tgbotapi.NewInlineKeyboardButtonData("Подбор", "search"),
	),
)

func (t *TGApi) StartTGBot() {
	bot, err := tgbotapi.NewBotAPI("5708271948:AAHGJdS5oMHmFEwiSITF0Vsld8LFGtRRA5A")
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	checkReset := true
	// Loop through each update.
	for update := range updates {
		if checkReset {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Материалы для языка программирования")
			msg.ReplyMarkup = numericKeyboard
			// Send the message.
			if _, err = bot.Send(msg); err != nil {
				panic(err)
			}

			checkReset = false
		} else if update.Message != nil {
			// Construct a new message from the given chat ID and containing
			// the text that we received.
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Пожалуйста, выберите один из вариантов ответа.")

			// Send the message.
			if _, err = bot.Send(msg); err != nil {
				panic(err)
			}
		} else if update.CallbackQuery != nil {
			if update.CallbackQuery.Data == "start" {
				msg := tgbotapi.NewMessage(update.CallbackQuery.Message.Chat.ID, "Возвращаемся в начало...")

				msg.ReplyMarkup = numericKeyboard
				if _, err := bot.Send(msg); err != nil {
					panic(err)
				}
			} else {
				question, err := t.delivery.GetAnswerFromUser(update.CallbackQuery.Data)
				if err != nil {
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, err.Error())
					if _, err := bot.Send(msg); err != nil {
						panic(err)
					}
					panic(err)
				}
				// Respond to the callback query, telling Telegram to show the user
				// a message with the data received.
				callback := tgbotapi.NewCallback(update.CallbackQuery.ID, update.CallbackQuery.Data)
				if _, err := bot.Request(callback); err != nil {
					panic(err)
				}

				args := question[update.CallbackQuery.Data]

				// And finally, send a message containing the data received.
				msg := tgbotapi.NewMessage(update.CallbackQuery.Message.Chat.ID, args[5]+"\n"+args[0])
				msg.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(
					tgbotapi.NewInlineKeyboardRow(
						tgbotapi.NewInlineKeyboardButtonData(args[1], args[3]),
						tgbotapi.NewInlineKeyboardButtonData(args[2], args[4]),
						tgbotapi.NewInlineKeyboardButtonData("В начало", "start"),
					))
				if _, err := bot.Send(msg); err != nil {
					panic(err)
				}
			}
		}
	}
}
