package mysql

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

func NewDBConnection(cfg Cfg) (*sql.DB, error) {
	db, err := sql.Open(cfg.GetDriverName(), cfg.GetDSN())
	if err != nil {
		return &sql.DB{}, err
	}

	if err := db.Ping(); err != nil {
		return &sql.DB{}, err
	}

	return db, nil
}
