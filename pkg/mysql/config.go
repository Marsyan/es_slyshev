package mysql

type Cfg struct {
	User       string
	Password   string
	Protocol   string
	Host       string
	Port       string
	DBName     string
	DriverName string
}

func (c *Cfg) GetDSN() string {
	return c.User + ":" + c.Password + "@" + c.Protocol + "(" + c.Host + ":" + c.Port + ")/" + c.DBName
}

func (c *Cfg) GetDriverName() string {
	return c.DriverName
}

// root:dbpass@tcp(127.0.0.1:3306)/es_slyshev
