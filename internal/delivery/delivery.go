package delivery

import (
	"gitlab.com/Marsyan/es_slyshev/internal/service"
	"gitlab.com/Marsyan/es_slyshev/internal/types"
)

type Delivery struct {
	service *service.Service
}

func NewDelivery(service *service.Service) *Delivery {
	return &Delivery{service: service}
}

func (d *Delivery) GetAnswerFromUser(data string) (types.Question, error) {
	var info types.Question
	switch data {
	case "search":
		info = d.search(data)
	case "yes1":
		info = d.yes1(data)
	case "no1":
		info = d.no1(data)
	case "yes2":
		info = d.yes2(data)
	case "no2":
		info = d.no2(data)
	case "yes3":
		info = d.yes3(data)
	case "no3":
		info = d.no3(data)
	case "yes4":
		info = d.yes4(data)
	case "no4":
		info = d.no4(data)
	case "yes5":
		info = d.yes5(data)
	case "no5":
		info = d.no5(data)
	case "yes6":
		info = d.yes6(data)
	case "no6":
		info = d.no6(data)
	case "yes7":
		info = d.yes7(data)
	case "no7":
		info = d.no7(data)
	case "yes8":
		info = d.yes8(data)
	case "no8":
		info = d.no8(data)
	case "yes9":
		info = d.yes9(data)
	case "no9":
		info = d.no9(data)
	case "yes10":
		info = d.yes10(data)
	case "no10":
		info = d.no10(data)
	case "yes11":
		info = d.yes11(data)
	case "no11":
		info = d.no11(data)
	case "yes12":
		info = d.yes12(data)
	case "no12":
		info = d.no12(data)
	case "yes13":
		info = d.yes13(data)
	case "no13":
		info = d.no13(data)
	case "yes14":
		info = d.yes14(data)
	case "no14":
		info = d.no14(data)
	case "yes15":
		info = d.yes15(data)
	case "no15":
		info = d.no15(data)
	case "yes16":
		info = d.yes16(data)
	case "no16":
		info = d.no16(data)
	case "yes17":
		info = d.yes17(data)
	case "no17":
		info = d.no17(data)
	case "yes18":
		info = d.yes18(data)
	case "no18":
		info = d.no18(data)
	case "yes19":
		info = d.yes19(data)
	case "no19":
		info = d.no19(data)
	case "yes20":
		info = d.yes20(data)
	case "no20":
		info = d.no20(data)
	case "yes21":
		info = d.yes21(data)
	case "no21":
		info = d.no21(data)
	}

	return info, nil
}

func (d *Delivery) search(data string) types.Question {
	question := "Вы новичок в программировании?"
	args := []string{question, "Да", "Нет", "yes1", "no1", "Мало данных"}

	return types.Question{data: args}
}

func (d *Delivery) yes1(data string) types.Question {
	d.service.Yes1()
	top := d.service.GetTop()
	question := "Вам нравится разбираться в непонятном?"
	args := []string{question, "Очень", "Не очень", "yes2", "no2", top}

	return types.Question{data: args}
}

func (d *Delivery) no1(data string) types.Question {
	d.service.No1()
	top := d.service.GetTop()
	question := "Вам нравится разбираться в непонятном?"
	args := []string{question, "Очень", "Не очень", "yes2", "no2", top}

	return types.Question{data: args}
}

func (d *Delivery) yes2(data string) types.Question {
	d.service.Yes2()
	top := d.service.GetTop()
	question := "Насколько для вас важны возможности языка?"
	args := []string{question, "Важны", "Не важны", "yes3", "no3", top}

	return types.Question{data: args}
}

func (d *Delivery) no2(data string) types.Question {
	d.service.No2()
	top := d.service.GetTop()
	question := "Насколько для вас важны возможности языка?"
	args := []string{question, "Важны", "Не важны", "yes3", "no3", top}

	return types.Question{data: args}
}

func (d *Delivery) yes3(data string) types.Question {
	d.service.Yes3()
	top := d.service.GetTop()
	question := "Чтобы вы выбрали из предложенных вариантов ответов в этом вопросе?"
	args := []string{question, "Быстродействие", "Компилируемость", "yes4", "no4", top}

	return types.Question{data: args}
}

func (d *Delivery) no3(data string) types.Question {
	d.service.No3()
	top := d.service.GetTop()
	question := "Чтобы вы выбрали из предложенных вариантов ответов в этом вопросе?"
	args := []string{question, "Быстродействие", "Компилируемость", "yes4", "no4", top}

	return types.Question{data: args}
}

func (d *Delivery) yes4(data string) types.Question {
	d.service.Yes4()
	top := d.service.GetTop()
	question := "А если бы мы предложили такие варианты ответов ?"
	args := []string{question, "Человекочитаемость", "Кроссплатформенность", "yes5", "no5", top}

	return types.Question{data: args}
}

func (d *Delivery) no4(data string) types.Question {
	d.service.No4()
	top := d.service.GetTop()
	question := "А если бы мы предложили такие варианты ответов в вопросе?"
	args := []string{question, "Человекочитаемость", "Кроссплатформенность", "yes5", "no5", top}

	return types.Question{data: args}
}

func (d *Delivery) yes5(data string) types.Question {
	d.service.Yes5()
	top := d.service.GetTop()
	question := "Вы бы хотели работать программистом в будущем?"
	args := []string{question, "Да, конечно!", "Нет, это для души", "yes6", "no6", top}

	return types.Question{data: args}
}

func (d *Delivery) no5(data string) types.Question {
	d.service.No5()
	top := d.service.GetTop()
	question := "Вы бы хотели работать программистом в будущем?"
	args := []string{question, "Да, конечно!", "Нет, это для души", "yes6", "no6", top}

	return types.Question{data: args}
}

func (d *Delivery) yes6(data string) types.Question {
	d.service.Yes6()
	top := d.service.GetTop()
	question := "Вы бы хотели быть универсальным разработчиком?"
	args := []string{question, "Да!", "Нет", "yes7", "no7", top}

	return types.Question{data: args}
}

func (d *Delivery) no6(data string) types.Question {
	d.service.No6()
	top := d.service.GetTop()
	question := "Вы бы хотели быть универсальным разработчиком?"
	args := []string{question, "Да!", "Нет", "yes7", "no7", top}

	return types.Question{data: args}
}

func (d *Delivery) yes7(data string) types.Question {
	d.service.Yes7()
	top := d.service.GetTop()
	question := "Сколько времени вы готовы уделять программированию?"
	args := []string{question, "Пару часов в день", "Не знаю точно", "yes8", "no8", top}

	return types.Question{data: args}
}

func (d *Delivery) no7(data string) types.Question {
	d.service.No7()
	top := d.service.GetTop()
	question := "Сколько времени вы готовы уделять программированию?"
	args := []string{question, "Пару часов в день", "Не знаю точно", "yes8", "no8", top}

	return types.Question{data: args}
}

func (d *Delivery) yes8(data string) types.Question {
	d.service.Yes8()
	top := d.service.GetTop()
	question := "Вы бы выбрали работу в тени (бек) или работу в свету (фронт)?"
	args := []string{question, "Backend", "Frontend", "yes9", "no9", top}

	return types.Question{data: args}
}

func (d *Delivery) no8(data string) types.Question {
	d.service.No8()
	top := d.service.GetTop()
	question := "Вы бы выбрали работу в тени (бек) или работу в свету (фронт)?"
	args := []string{question, "Backend", "Frontend", "yes9", "no9", top}

	return types.Question{data: args}
}

func (d *Delivery) yes9(data string) types.Question {
	d.service.Yes9()
	top := d.service.GetTop()
	question := "Нравится ли вам работать с базой данных?"
	args := []string{question, "Да", "нет", "yes10", "no10", top}

	return types.Question{data: args}
}

func (d *Delivery) no9(data string) types.Question {
	d.service.No9()
	top := d.service.GetTop()
	question := "Нравится ли вам работать с базой данных?"
	args := []string{question, "Да", "нет", "yes10", "no10", top}

	return types.Question{data: args}
}

func (d *Delivery) yes10(data string) types.Question {
	d.service.Yes10()
	top := d.service.GetTop()
	question := "Вы любите математику?"
	args := []string{question, "Да", "нет", "yes11", "no11", top}

	return types.Question{data: args}
}

func (d *Delivery) no10(data string) types.Question {
	d.service.No10()
	top := d.service.GetTop()
	question := "Вы любите математику?"
	args := []string{question, "Да", "нет", "yes11", "no11", top}

	return types.Question{data: args}
}

func (d *Delivery) yes11(data string) types.Question {
	d.service.Yes11()
	top := d.service.GetTop()
	question := "Какая операционная система вам больше нравится?"
	args := []string{question, "Windows", "Linux", "yes12", "no12", top}

	return types.Question{data: args}
}

func (d *Delivery) no11(data string) types.Question {
	d.service.No11()
	top := d.service.GetTop()
	question := "Какая операционная система вам больше нравится?"
	args := []string{question, "Windows", "Linux", "yes12", "no12", top}

	return types.Question{data: args}
}

func (d *Delivery) yes12(data string) types.Question {
	d.service.Yes12()
	top := d.service.GetTop()
	question := "Вы умеете работать в команде?"
	args := []string{question, "Да, люблю людей", "Работаю один", "yes13", "no13", top}

	return types.Question{data: args}
}

func (d *Delivery) no12(data string) types.Question {
	d.service.No12()
	top := d.service.GetTop()
	question := "Вы умеете работать в команде?"
	args := []string{question, "Да, люблю людей", "Работаю один", "yes13", "no13", top}

	return types.Question{data: args}
}

func (d *Delivery) yes13(data string) types.Question {
	d.service.Yes13()
	top := d.service.GetTop()
	question := "Как вы относитесь к тестированию написанного вами кода?"
	args := []string{question, "Положительно", "Отрицательно", "yes14", "no14", top}

	return types.Question{data: args}
}

func (d *Delivery) no13(data string) types.Question {
	d.service.No13()
	top := d.service.GetTop()
	question := "Как вы относитесь к тестированию написанного вами кода?"
	args := []string{question, "Положительно", "Отрицательно", "yes14", "no14", top}

	return types.Question{data: args}
}

func (d *Delivery) yes14(data string) types.Question {
	d.service.Yes14()
	top := d.service.GetTop()
	question := "Вам бы хотелось разрабатывать высоконагруженные сервисы?"
	args := []string{question, "Дааа", "Нееет", "yes15", "no15", top}

	return types.Question{data: args}
}

func (d *Delivery) no14(data string) types.Question {
	d.service.No14()
	top := d.service.GetTop()
	question := "Вам бы хотелось разрабатывать высоконагруженные сервисы?"
	args := []string{question, "Дааа", "Нееет", "yes15", "no15", top}

	return types.Question{data: args}
}

func (d *Delivery) yes15(data string) types.Question {
	d.service.Yes15()
	top := d.service.GetTop()
	question := "Вам бы хотелось разрабатывать мобильные приложения?"
	args := []string{question, "Конечно", "Ни в коем случае", "yes16", "no16", top}

	return types.Question{data: args}
}

func (d *Delivery) no15(data string) types.Question {
	d.service.No15()
	top := d.service.GetTop()
	question := "Вам бы хотелось разрабатывать мобильные приложения?"
	args := []string{question, "Конечно", "Ни в коем случае", "yes16", "no16", top}

	return types.Question{data: args}
}

func (d *Delivery) yes16(data string) types.Question {
	d.service.Yes16()
	top := d.service.GetTop()
	question := "Вы бы предпочли работу на дому или в офисе?"
	args := []string{question, "Офис", "Дом", "yes17", "no17", top}

	return types.Question{data: args}
}

func (d *Delivery) no16(data string) types.Question {
	d.service.No16()
	top := d.service.GetTop()
	question := "Вы бы предпочли работу на дому или в офисе?"
	args := []string{question, "Офис", "Дом", "yes17", "no17", top}

	return types.Question{data: args}
}

func (d *Delivery) yes17(data string) types.Question {
	d.service.Yes17()
	top := d.service.GetTop()
	question := "Знакомы ли вы с алгоритмами?"
	args := []string{question, "Да", "Нет", "yes18", "no18", top}

	return types.Question{data: args}
}

func (d *Delivery) no17(data string) types.Question {
	d.service.No17()
	top := d.service.GetTop()
	question := "Знакомы ли вы с алгоритмами?"
	args := []string{question, "Да", "Нет", "yes18", "no18", top}

	return types.Question{data: args}
}

func (d *Delivery) yes18(data string) types.Question {
	d.service.Yes18()
	top := d.service.GetTop()
	question := "Знакомы ли вы со структурами данных?"
	args := []string{question, "Да", "Нет", "yes19", "no19", top}

	return types.Question{data: args}
}

func (d *Delivery) no18(data string) types.Question {
	d.service.No18()
	top := d.service.GetTop()
	question := "Знакомы ли вы со структурами данных?"
	args := []string{question, "Да", "Нет", "yes19", "no19", top}

	return types.Question{data: args}
}

func (d *Delivery) yes19(data string) types.Question {
	d.service.Yes19()
	top := d.service.GetTop()
	question := "Вы считаете себя продвинутым пользователем компьютера?"
	args := []string{question, "Да, я хакер", "Нет", "yes20", "no20", top}

	return types.Question{data: args}
}

func (d *Delivery) no19(data string) types.Question {
	d.service.No19()
	top := d.service.GetTop()
	question := "Вы считаете себя продвинутым пользователем компьютера?"
	args := []string{question, "Да, я хакер", "Нет", "yes20", "no20", top}

	return types.Question{data: args}
}

func (d *Delivery) yes20(data string) types.Question {
	d.service.Yes20()
	top := d.service.GetTop()
	question := "Вы знаете что такое командная строка?"
	args := []string{question, "Yes", "No", "yes21", "no21", top}

	return types.Question{data: args}
}

func (d *Delivery) no20(data string) types.Question {
	d.service.No20()
	top := d.service.GetTop()
	question := "Вы знаете что такое командная строка?"
	args := []string{question, "Yes", "No", "yes21", "no21", top}

	return types.Question{data: args}
}

func (d *Delivery) yes21(data string) types.Question {
	d.service.Yes21()
	top := d.service.GetTop()
	question := "Это топ подходящих для вас языков программирования!"
	args := []string{question, "Ого", "Круто", "s", "s", top}

	return types.Question{data: args}
}

func (d *Delivery) no21(data string) types.Question {
	d.service.No21()
	top := d.service.GetTop()
	question := "Это топ подходящих для вас языков программирования!"
	args := []string{question, "Ого", "Круто", "s", "s", top}

	return types.Question{data: args}
}
