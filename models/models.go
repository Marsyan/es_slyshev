package models

type Language struct {
	Ease                  float64 // Простота
	Capabilities          float64 // Возможности
	HumanReadability      float64 // Человекочитаемость
	BreadthOfApplications float64 // Широта применения
	Modernity             float64 // Современность
	EntryThreshold        float64 // Порог вхождения
	Compilability         float64 // компилируемость
	Performance           float64 // Быстродействие
	Crossplatform         float64 // Кросплатформенность
	DemandInTheMarket     float64 // Востребованность на рынке
}
